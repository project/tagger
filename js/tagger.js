
Drupal.behaviors.tagger = function(context) {
  // get all attributes of span tag
  $('span.tags_suggest').each ( function() {
    var a = $('<a>' + this.innerHTML + '</a>')
    .attr('href', '#')
    .attr('id', $(this).attr('id'))
    .addClass($(this).attr('class'))
    .bind('click', function(event) {
      event.preventDefault();
      var input = $(this).parents(".form-item").children('input');
      if ($(this).parent().attr('class') == 'tags_suggest_batax_wrapper') {
        var text = $(this).text() + ':' + $(this).attr('id').substr(17);
      } else {
        var text = $(this).text();
      }
      if (((', ' + input.val() + ',').indexOf(', ' + text + ',') < 0) && ((', ' + input.val() + ',').indexOf(', "' + text + '",') < 0)) {
        if ((input.val()).length > 0) {
          input.val(input.val() + ', ');
        }
        input.val(input.val() + text);
      }
    }); // end bind
    $(this).before(a).remove();
  }); // end span.suggestedterm
} // end build_links
