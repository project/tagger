Drupal.behaviors.taggerVocabAdmin = function(context) {
  checkStatus();
  $('#edit-use-tagger-for-vocabulary').click(function(){
    checkStatus();
  })
  function checkStatus() {
    if ($('#edit-use-tagger-for-vocabulary').is(':checked')) {
      $('#edit-threshold').removeAttr('disabled');
    } else {
      $('#edit-threshold').attr('disabled', true);
    }
  }
} 